package org.xtext.example.mydsl.tests

import ourcode.Interpreter

import com.google.inject.Inject
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith
import org.xtext.example.mydsl.aGMLJSon.Model
import jdk.nashorn.internal.runtime.JSONListAdapter

@ExtendWith(InjectionExtension)
@InjectWith(AGMLJSonInjectorProvider)
class AGMLJSonInterpretTest {

	@Inject
	ParseHelper<Model> parseHelper

	@Test
	def void Test1() {
		val result = parseHelper.parse('''			
			open "test1.agmljson" in {
				print(self);
				print(
					select(self, "glossary"/"GlossDiv"/"title")
				);
				print(self);
				select(self, "glossary");
				print(self);
			};
		''')
		Assertions.assertNotNull(result)
		val errors = result.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		Interpreter.interpret(result)		
	}
	
	@Test
	def void Test2() {
		val result = parseHelper.parse('''			
			{"a": [1, 2, 3]};
			print(sum(self, "a"));
		''')
		Assertions.assertNotNull(result)
		val errors = result.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		Interpreter.interpret(result)		
	}
	
	@Test
	def void Test3() {
		val result = parseHelper.parse('''			
			{"a": [1, 2, 5]};
			print(product(self, "a"));
		''')
		Assertions.assertNotNull(result)
		val errors = result.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		Interpreter.interpret(result)		
	}
}