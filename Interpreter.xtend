package ourcode

import org.xtext.example.mydsl.aGMLJSon.*
import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter
import java.io.IOException
import java.util.UUID
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.xtext.example.mydsl.AGMLJSonStandaloneSetup

// Class to parse a JSON file as a Model of our DSL. Json grammar is included into our DSL's.
// ParserHelper is not available outside of tests ,so this this trick is needed.
// Adapted from : https://gist.github.com/acherm/bf7896a81d8db2c3726e82e8d4d921ec
class JSONLoader {
	def static JsonValue loadJSON(String content) {		
		try {
			// first the content is written into a temporary file
		    val File temp = File.createTempFile("agmlfile" + UUID.randomUUID(), ".agml"); 	    
		    val URI uri = URI.createFileURI(temp.getAbsolutePath());		
		    val BufferedWriter bw = new BufferedWriter(new FileWriter(temp));
		    bw.write(content);
		    bw.close();
		    // second load the resource	
		    AGMLJSonStandaloneSetup.doSetup();	
		    val Resource res = new ResourceSetImpl().getResource(uri, true);		
		    var JsonValue value = res.getContents().get(0) as JsonValue; 
		    return value;
		}
		
		catch (IOException e) {
		     println("Error loading AGML model")
		     return null
		}	   
	}
	
	def static JsonValue loadJSON(URI uri) {		
		try {			
		    AGMLJSonStandaloneSetup.doSetup();	
		    val Resource res = new ResourceSetImpl().getResource(uri, true);		
		    var JsonValue value = ((res.getContents().get(0) as Model).satements.get(0) as Const).value as JsonValue
		    return value;
		}
		
		catch (IOException e) {
			println("Error loading AGML model")
		    return null 
		}	   
	}
}

class Interpreter {
	def static interpret(Model m) {
		println("Starting interpreter with an empty context.")
		interpretModel(m, null)
		println("Done !")
	}
	
	def static JsonValue interpretModel(Model m, JsonValue context) {
		var JsonValue current = context
		for (Statement s : m.satements) {
			current = interpretStmt(s, current)
		}
		return current
	}
	
	def static dispatch JsonValue interpretStmt(Print s, JsonValue context) {
		println(PrettyPrinter.prettyPrint(
			interpretStmt(s.value, context), 0)
		)
		return context
	}
	
	def static JsonValue interpretBlock(Block b) {
		val URI uri = URI.createFileURI(
			(new File(b.block_name)).absolutePath
		)
		var JsonValue jsonContext = JSONLoader.loadJSON(uri)
		
		return interpretModel(b.content, jsonContext)
	}
	
	def static dispatch JsonValue interpretStmt(Value v, JsonValue context) {
		return switch v {
			Const : v.value
			Querry : interpretQuery(v, context)
			Self : context
			Block : interpretBlock(v)
		}
	}
	
	def static JsonValue getValueAtPath(Path p, JsonValue target) {
		var JsonValue current
		
		try {
			current = target as JsonObject
		} catch (ClassCastException e) {
			throw new IncorrectQueryException("A Path can only be applied on a JsonObject")
		}
		
		// for each key in the path, we go down into the json tree
		for (var i = 0; i < p.path.size; i++) {
			var members_itr = (current as JsonObject).member.members.iterator
			var found = false
			var key = p.path.get(i)
						
			while (members_itr.hasNext && !found) {
				var cur_member = members_itr.next
				if (cur_member.id.equals(key)) {
					found = true
					// if this isn't the last element of the path, then we need to cast it into a JsonObject
					if (i != p.path.size-1) {
						try {
							current = cur_member.value as JsonObject
						} catch (Exception e) {
							throw new IncorrectQueryException("A path can only be applied on a JsonObject")
						}
					} else {
						current = cur_member.value
					}
				}
			}
			if (!found) { 
				throw new IncorrectQueryException("Json tree traversal ended without finding a matching key.")
			}
		}
		return current
	}
	
	def static dispatch JsonValue interpretQuery(Select s, JsonValue context) {
		var target = interpretStmt(s.from, context)
		return getValueAtPath(s.where, target)
	}
	
	def static dispatch JsonValue interpretQuery(Sum s, JsonValue context) {
		var target = interpretStmt(s.from, context)
		var JsonArray array
		
		try {
			array = getValueAtPath(s.where, target) as JsonArray
		} catch (ClassCastException e) {
			throw new IncorrectQueryException("Target value in JSON file wasn't an array")
		}
		
		return switch array.element.elements.get(0) {
			JsonNumber : operateInt(array, [x, y | x+y], 0)
			JsonString : operateString(array, [x, y | x+y])
			default : throw new IncorrectQueryException("Sum can only be applied on int or string")
		}
	}
	
	def static dispatch JsonValue interpretQuery(Product p, JsonValue context) {
		var target = interpretStmt(p.from, context)
		var JsonArray array
		
		try {
			array = getValueAtPath(p.where, target) as JsonArray
		} catch (ClassCastException e) {
			throw new IncorrectQueryException("Target value in JSON file wasn't an array")
		}
		
		return switch array.element.elements.get(0) {
			JsonNumber : operateInt(array, [x, y | x*y], 1)
			default : throw new IncorrectQueryException("product can only be applied on int or string")
		}
	}
	
	def static JsonValue operateInt(JsonArray array, (int, int)=>int function, int start) {
		var sum = start
		for (element : array.element.elements) {
			try {
				sum = function.apply(sum, (element as JsonNumber).int)
			} catch (ClassCastException e) {
				throw new IncorrectQueryException("Operation can only be applied on homogenous arrays") 
			}
		}
		var res = AGMLJSonFactory.eINSTANCE.createJsonNumber
		res.int = sum
		return res
	}
	
	def static JsonValue operateString(JsonArray array, (String, String)=>String function) {
		var sum = ""
		for (element : array.element.elements) {
			try {
				sum = function.apply(sum, (element as JsonString).string)
			} catch (ClassCastException e) {
				throw new IncorrectQueryException("Operation can only be applied on homogenous arrays") 
			}
		}
		var res = AGMLJSonFactory.eINSTANCE.createJsonString
		res.string = sum
		return res
	}
	
}

class IncorrectQueryException extends Exception { 
    new(String errorMessage) {
        super(errorMessage);
    }
}