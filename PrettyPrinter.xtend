package ourcode

import org.xtext.example.mydsl.aGMLJSon.Model
import org.xtext.example.mydsl.aGMLJSon.Block
import org.xtext.example.mydsl.aGMLJSon.Write
import org.xtext.example.mydsl.aGMLJSon.Print
import org.xtext.example.mydsl.aGMLJSon.Export
import org.xtext.example.mydsl.aGMLJSon.Const
import org.xtext.example.mydsl.aGMLJSon.JsonObject
import org.xtext.example.mydsl.aGMLJSon.JsonMember
import org.xtext.example.mydsl.aGMLJSon.JsonMembers
import org.xtext.example.mydsl.aGMLJSon.JsonTrue
import org.xtext.example.mydsl.aGMLJSon.JsonString
import org.xtext.example.mydsl.aGMLJSon.JsonFalse
import org.xtext.example.mydsl.aGMLJSon.JsonNull
import org.xtext.example.mydsl.aGMLJSon.JsonArray
import org.xtext.example.mydsl.aGMLJSon.JsonNumber
import org.xtext.example.mydsl.aGMLJSon.JsonElements
import org.xtext.example.mydsl.aGMLJSon.Querry
import org.xtext.example.mydsl.aGMLJSon.Insert
import org.xtext.example.mydsl.aGMLJSon.Self
import org.xtext.example.mydsl.aGMLJSon.Remove
import org.xtext.example.mydsl.aGMLJSon.Select
import org.xtext.example.mydsl.aGMLJSon.Project
import org.xtext.example.mydsl.aGMLJSon.Sum
import org.xtext.example.mydsl.aGMLJSon.Product
import org.xtext.example.mydsl.aGMLJSon.Update

class PrettyPrinter {
	
	def static String prettyPrintIndent(int depth) {
		if (depth == 0) return "";
		return "  "+prettyPrintIndent(depth-1);
	}
	
	def static dispatch String prettyPrint(JsonString jsonstring, int depth) {
		return "\" " + jsonstring.getString() + " \"";
	}

	def static dispatch String prettyPrint(JsonNumber jsonnumber, int depth) {
		return jsonnumber.getInt().toString();
	}
	
	def static dispatch String prettyPrint(JsonTrue jsontrue, int depth) {
		return "TRUE";
	}
	
	def static dispatch String prettyPrint(JsonFalse jsonfalse, int depth) {
		return "FALSE";
	}
	
	def static dispatch String prettyPrint(JsonNull jsonnull, int depth) {
		return "NULL";
	}
	
	def static dispatch String prettyPrint(JsonElements jsonelements, int depth) {
		val elements = jsonelements.getElements().listIterator();
		if (!elements.hasNext()) return ""
		var out = prettyPrintIndent(depth) + elements.next().prettyPrint(depth)
		while(elements.hasNext()){
			out += ",\n" + prettyPrintIndent(depth) + elements.next().prettyPrint(depth);
		}
		return out+"\n";
	}
	
	def static dispatch String prettyPrint(JsonArray jsonarray, int depth) {
		var out = "["
		if (jsonarray.getElement === null) out += "]\n"
		else out = out + "\n" +jsonarray.getElement.prettyPrint(depth+1)
				 + prettyPrintIndent(depth) + "]"
		return out;
	}
		
	def static dispatch String prettyPrint(JsonMember jsonmember, int depth) {
		var out = "\" " + jsonmember.getId() + " \" : "
		+ jsonmember.getValue().prettyPrint(depth);
		return out;
	}
	
	def static dispatch String prettyPrint(JsonMembers jsonmembers, int depth) {		
		val members = jsonmembers.getMembers().listIterator();
		if (!members.hasNext()) return ""
		var out = prettyPrintIndent(depth) + members.next().prettyPrint(depth)
		while(members.hasNext()){
			out += ",\n" + prettyPrintIndent(depth) + members.next().prettyPrint(depth);
		}
		return out+"\n";
	} 
	
	def static dispatch String prettyPrint(JsonObject jsonobject, int depth) {
		var out = "{"
		if (jsonobject.getMember() === null) out += "}"
		else out = out + "\n" +jsonobject.getMember.prettyPrint(depth+1)
				 + prettyPrintIndent(depth) + "}"
		return out;
	}
		
	def static dispatch String prettyPrint(Self slf, int depth) {
		return "Self";
	}
	
	def static dispatch String prettyPrint(Remove remove, int depth) {
		return "remove(" + remove.getFrom().prettyPrint(depth) + ", "
		 + remove.getWhere() + ")"		
	}
	
	def static dispatch String prettyPrint(Update update, int depth) {
		return "update(" + update.getFrom().prettyPrint(depth) + ", "
		 + update.getNew_value().prettyPrint(depth) + ", "
		 + update.getWhere() + ")"		
	}
	
	def static dispatch String prettyPrint(Insert insert, int depth) {
		return "insert(" + insert.getFrom().prettyPrint(depth) + ", "
		 + insert.getNew_value().prettyPrint(depth) + ")"		
	}
	
	def static dispatch String prettyPrint(Product product, int depth) {
		return "product(" + product.getFrom().prettyPrint(depth) + ", "
		 + product.getWhere() + ")"		
	}
	
	def static dispatch String prettyPrint(Sum sum, int depth) {
		return "sum(" + sum.getFrom().prettyPrint(depth) + ", "
		 + sum.getWhere() + ")"		
	}
	
	def static dispatch String prettyPrint(Project project, int depth) {
		return "select(" + project.getFrom().prettyPrint(depth) + ", "
		 + project.getWanted_value().prettyPrint(depth) + ", "
		 + project.getWhere() + ")"		
	}
	
	def static dispatch String prettyPrint(Select select, int depth) {
		return "select(" + select.getFrom().prettyPrint(depth) + ", "
		 + select.getWhere() + ")"		
	}
	
	def static dispatch String prettyPrint(Querry querry, int depth) {
		return "TODO"
	}
	
	def static dispatch String prettyPrint(Const const, int depth) {
		return const.getValue().prettyPrint(depth);
	}
	
	def static dispatch String prettyPrint(Block block, int depth) {
		var out = prettyPrintIndent(depth) + block.getBlock_name() " : {\n" 
		+ block.getContent().prettyPrint(depth+1)
		+ prettyPrintIndent(depth) + ");\n";
		 return out;		
	}
	
	def static dispatch String prettyPrint(Write write, int depth) {
		var out = prettyPrintIndent(depth) + "write(\"" 
		+ write.getFile_name()
		+ prettyPrintIndent(depth) + "\");\n";
		 return out;
	}
	
	def static dispatch String prettyPrint(Print print, int depth) {
		var out = prettyPrintIndent(depth) + "print(" 
		+ print.getValue().prettyPrint(depth+1)
		+ prettyPrintIndent(depth) + ");\n";
		 return out;	
	}
	
	def static dispatch prettyPrint(Export export, int depth) {
		var out = prettyPrintIndent(depth) + "export(\"" 
		+ export.getFile_name()
		+ prettyPrintIndent(depth) + "\");\n";
		 return out;
	}
	
	def static String prettyPrint(Model ParsingRes) {
		var out = new String("");
		val statements = ParsingRes.getSatements().iterator();
		while(statements.hasNext()){
			out += statements.next().prettyPrint(0);
		}
		return out;
	}
}