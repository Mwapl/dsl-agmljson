package ourcode

import org.xtext.example.mydsl.aGMLJSon.Model
import org.xtext.example.mydsl.aGMLJSon.Block
import org.xtext.example.mydsl.aGMLJSon.Write
import org.xtext.example.mydsl.aGMLJSon.Print
import org.xtext.example.mydsl.aGMLJSon.Export
import org.xtext.example.mydsl.aGMLJSon.Const
import org.xtext.example.mydsl.aGMLJSon.JsonObject
import org.xtext.example.mydsl.aGMLJSon.JsonMember
import org.xtext.example.mydsl.aGMLJSon.JsonMembers
import org.xtext.example.mydsl.aGMLJSon.JsonTrue
import org.xtext.example.mydsl.aGMLJSon.JsonString
import org.xtext.example.mydsl.aGMLJSon.JsonFalse
import org.xtext.example.mydsl.aGMLJSon.JsonNull
import org.xtext.example.mydsl.aGMLJSon.JsonArray
import org.xtext.example.mydsl.aGMLJSon.JsonNumber
import org.xtext.example.mydsl.aGMLJSon.JsonElements
import org.xtext.example.mydsl.aGMLJSon.Path
import org.xtext.example.mydsl.aGMLJSon.Insert
import org.xtext.example.mydsl.aGMLJSon.Self
import org.xtext.example.mydsl.aGMLJSon.Remove
import org.xtext.example.mydsl.aGMLJSon.Select
import org.xtext.example.mydsl.aGMLJSon.Project
import org.xtext.example.mydsl.aGMLJSon.Sum
import org.xtext.example.mydsl.aGMLJSon.Product
import org.xtext.example.mydsl.aGMLJSon.Update

class PythonCompiler {
		
	def static dispatch String pythonCompile(JsonString jsonstring) {
		return "\"" + jsonstring.getString() + "\"";
	}

	def static dispatch String pythonCompile(JsonNumber jsonnumber) {
		return jsonnumber.getInt().toString();
	}
	
	def static dispatch String pythonCompile(JsonTrue jsontrue) {
		return "true";
	}
	
	def static dispatch String pythonCompile(JsonFalse jsonfalse) {
		return "false";
	}
	
	def static dispatch String pythonCompile(JsonNull jsonnull) {
		return "null";
	}
	
	def static dispatch String pythonCompile(JsonElements jsonelements) {
		val elements = jsonelements.getElements().listIterator();
		if (!elements.hasNext()) return ""
		var out = elements.next().pythonCompile()
		while(elements.hasNext()){
			out += ", " + elements.next().pythonCompile();
		}
		return out+"";
	}
	
	def static dispatch String pythonCompile(JsonArray jsonarray) {
		var out = "["
		if (jsonarray.getElement === null) out += "]"
		else out = out +jsonarray.getElement.pythonCompile() + "]"
		return out;
	}
		
	def static dispatch String pythonCompile(JsonMember jsonmember) {
		var out = "\"" + jsonmember.getId() + "\" : "
		+ jsonmember.getValue().pythonCompile();
		return out;
	}
	
	def static dispatch String pythonCompile(JsonMembers jsonmembers) {
		val members = jsonmembers.getMembers().listIterator();
		if (!members.hasNext()) return ""
		var out = members.next().pythonCompile()
		while(members.hasNext()){
			out += ", " + members.next().pythonCompile();
		}
		return out;
	} 
	
	def static dispatch String pythonCompile(JsonObject jsonobject) {
		var out = "{"
		if (jsonobject.getMember() === null) out += "}"
		else out = out + jsonobject.getMember.pythonCompile() + "}"
		return out;
	}
		
	def static dispatch String pythonCompileVl(Self slf) {
		return "stack.append(copy.deepcopy(stack[top]))\n";
	}
	
	def static dispatch String pythonCompile(Path path) {
		var out = "["
		val nodes = path.getPath().iterator()
		out += "\"" + nodes.next() + "\""
		while(nodes.hasNext()){
			out += ", \"" + nodes.next() + "\""
		}
		return out + "]";		
	}
	
	def static dispatch String pythonCompileVl(Remove remove) {
		var out = remove.getFrom().pythonCompileVl() 
		out += "stack[-1].select(" + remove.getWhere().pythonCompile() + ")\n"
		return out;	
	}
	
	def static dispatch String pythonCompileVl(Update update) {
		var out = update.getFrom().pythonCompileVl()
		out += update.getNew_value().pythonCompileVl() 
		out += "stack[-2].update(" + update.getWhere().pythonCompile() + ", stack[-1].getValue())\n"
		out += "stack.pop()"
		return out;	
	}
	
	def static dispatch String pythonCompileVl(Insert insert) {
		var out = insert.getFrom().pythonCompileVl()
		out += insert.getNew_value().pythonCompileVl()
		out += "stack[-2].insert(" + insert.getWhere().pythonCompile() + ", stack[-1].getValue())\n"
		out += "stack.pop()"
		return out;	
	}
	
	def static dispatch String pythonCompileVl(Product product) {
		var out = product.getFrom().pythonCompileVl() 
		out += "stack[-1].select(" + product.getWhere().pythonCompile() + ")\n"
		return out;	
	}
	
	def static dispatch String pythonCompileVl(Sum sum) {
		var out = sum.getFrom().pythonCompileVl()
		out += "stack[-1].select(" + sum.getWhere().pythonCompile() + ")\n"
		return out;		
	}
	
	def static dispatch String pythonCompileVl(Project project) {
		var out = project.getFrom().pythonCompileVl()
		out += project.getWanted_value().pythonCompileVl() 
		out += "stack[-2].project(" + project.getWhere().pythonCompile() + ", stack[-1].getValue())\n"
		out += "stack.pop()"
		return out;
	}
	
	def static dispatch String pythonCompileVl(Select select) {
		var out = select.getFrom().pythonCompileVl() 
		out += "stack[-1].select(" + select.getWhere().pythonCompile() + ")\n"
		return out;
	}
	
	
	def static dispatch String pythonCompileVl(Const const) {
		var out = "stack.append(JsonObj())\n"
		out += "stack[-1].decode('" + const.getValue().pythonCompile() + "')\n";
		return out
	}
	
	def static dispatch String pythonCompileVl(Block block) {
		var out = "stack.append(JsonObj())\n"
		out += "stack[-1].open('"+ block.getBlock_name() +"')\n"
		out += "top += 1\n"
 		out += block.getContent().pythonCompileBis();
		out += "top -= 1\n"
		return out;			
	}
	
	def static dispatch String pythonCompileStmt(Self slf) {
		return "stack[top] = stack[top]\n";
	}
	
	def static dispatch String pythonCompileStmt(Remove remove) {
		var out = remove.pythonCompileVl()
		out += "tmp_res = stack.pop()\n"
		out += "stack[top] = tmp_res\n"
		return out;
	}
	
	def static dispatch String pythonCompileStmt(Update update) {
		var out = update.pythonCompileVl()
		out += "tmp_res = stack.pop()\n"
		out += "stack[top] = tmp_res\n"
		return out;
	}
	
	def static dispatch String pythonCompileStmt(Insert insert) {
		var out = insert.pythonCompileVl()
		out += "tmp_res = stack.pop()\n"
		out += "stack[top] = tmp_res\n"
		return out;
	}
	
	def static dispatch String pythonCompileStmt(Product product) {
		var out = product.pythonCompileVl()
		out += "tmp_res = stack.pop()\n"
		out += "stack[top] = tmp_res\n"
		return out;
	}
	
	def static dispatch String pythonCompileStmt(Sum sum) {
		var out = sum.pythonCompileVl()
		out += "tmp_res = stack.pop()\n"
		out += "stack[top] = tmp_res\n"
		return out;
	}
	
	def static dispatch String pythonCompileStmt(Project project) {
		var out = project.pythonCompileVl()
		out += "tmp_res = stack.pop()\n"
		out += "stack[top] = tmp_res\n"
		return out;
	}
	
	def static dispatch String pythonCompileStmt(Select select) {
		var out = select.pythonCompileVl()
		out += "tmp_res = stack.pop()\n"
		out += "stack[top] = tmp_res\n"
		return out;
	}
	
	
	def static dispatch String pythonCompileStmt(Const const) {
		var out = const.pythonCompileVl()
		out += "tmp_res = stack.pop()\n"
		out += "stack[top] = tmp_res\n"
		return out;
	}
	
	def static dispatch String pythonCompileStmt(Block block) {
		var out = "stack.append(JsonObj())\n"
		out += "stack[-1].load('"+ block.getBlock_name() +"')\n"
		out += "top += 1\n"
		out += block.getContent().pythonCompileBis();
		out += "blck_res = stack.pop()\n"
		out += "top -= 1\n"
		out += "stack[top] = blck_res\n"
		return out;			
	}
	
	def static dispatch String pythonCompileStmt(Write write) {
		return "stack[top].write('" + write.getFile_name() + "')\n"
	}
	
	def static dispatch String pythonCompileStmt(Print print) {
		var out = print.getValue().pythonCompileVl()
		out += "print(json.dumps(stack.pop().getValue(), sort_keys=True, indent=4))\n"
		 return out;	
	}
	
	def static dispatch String pythonCompileStmt(Export export) {
		var out = "stack[top].exportCSV('" + export.getFile_name() + "')\n";
		return out;
	}
	
	def static String pythonCompileBis(Model ParsingRes) {
		var out = ""
		val statements = ParsingRes.getSatements().iterator();
		while(statements.hasNext()){
			out += statements.next().pythonCompileStmt();
		}
		return out;
	}
		
	
	def static dispatch String pythonCompile(Model ParsingRes) {
		var out = 
		'''
		##This python script was compiled from AGMLJson DSL by our compiler
		##Global stuff
		import json, csv, copy
		class JsonObj:
			def __init__(self):
				self.JsonValue = {}
			def load(self, filename):
				with open(filename, 'r') as json_file:
					self.JsonValue = json.load(json_file)
			def write(self, filename):
				with open(filename, 'w') as json_file:
					json.dump(self.JsonValue, json_file)
			def getValue(self):
				return self.JsonValue
			def exportCSV(self, filename):
				with open(filename, 'w') as csv_file:
					out = csv.writer(csv_file)
					out.writerow(self.JsonValue.keys())
					for row in self.JsonValue.values():
						out.writerow(row.values())
			def decode(self, json_string):
				self.JsonValue = json.loads(json_string)
			def getPath(self, path):
				curr = self.JsonValue
				for node in path:
					if node in curr:
						curr = curr[node]
					else:
						print("\e[31mERROR\e[31m : There is no such path {} in {}".format(path, self.JsonValue))
						exit(1)
				return curr
			def remove(self, path):
				obj = self.getPath(path)
				del obj
			def update(self, path, new_value):
				self.getPath(path).update(new_value)
			def insert(self, path, new_value):
				self.getPath(path).update(new_value)
			def sum(self, path):
				array = self.getPath(path)
				sum = 0
				for x in array:
					sum += x
				self.JsonValue = {"ProductResult" : sum}
			def product(self, path):
				arr = self.getPath(path)
				prod = 1
				for x in array:
					prod *= x
				self.JsonValue = {"ProductResult" : prod}
			def project(self, path, wanted_value):
				wo = self.getPath(path)
				for key in wo.keys():
					if wo[key] != wanted_value:
						del wo[key]
			def select(self, path):
				self.JsonValue = self.getPath(path)
		stack = [JsonObj()]
		top = 0
		##Actual Code begins here
		'''
		val statements = ParsingRes.getSatements().iterator();
		while(statements.hasNext()){
			out += statements.next().pythonCompileStmt();
		}
		return out;
	}
}