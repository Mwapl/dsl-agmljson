package org.xtext.example.mydsl.tests

import ourcode.PythonCompiler

import com.google.inject.Inject
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith
import org.xtext.example.mydsl.aGMLJSon.Model
import java.nio.file.Files
import java.io.FileOutputStream
import java.io.InputStreamReader
import java.io.BufferedReader

@ExtendWith(InjectionExtension)
@InjectWith(AGMLJSonInjectorProvider)
class AGMLJSonCompileTest {

	@Inject
	ParseHelper<Model> parseHelper
	
	@Test
	def void ListJSON() {
		val result = parseHelper.parse('''
			print({"false&null" : [false, null]});
		''')
		Assertions.assertNotNull(result)
		val errors = result.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		println(PythonCompiler.pythonCompile(result));
	}
	
	@Test
	def void HelloWorld() {
		val result = parseHelper.parse('''
			print({"bonjour" : true});
		''')
		Assertions.assertNotNull(result)
		val errors = result.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		println(PythonCompiler.pythonCompile(result));
	}

	
	
	
	@Test
	def void MultipleStatements() {
		val result = parseHelper.parse('''			
			print({});
			print(
				insert({}, { "HelloWorld" : null })
				);			
		''')
		Assertions.assertNotNull(result)
		val errors = result.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		println(PythonCompiler.pythonCompile(result));		
		}
	
	@Test
	def void HardJSON() {
		val result = parseHelper.parse('''
			{
			    "glossary": {
			        "title": "example glossary",
					"GlossDiv": {
			            "title": "S",
						"GlossList": {
			                "GlossEntry": {
			                    "ID": "SGML",
								"SortAs": "SGML",
								"GlossTerm": "Standard Generalized Markup Language",
								"Acronym": "SGML",
								"Abbrev": "ISO 8879:1986",
								"GlossDef": {
			                        "para": "A meta-markup language, used to create markup languages such as DocBook.",
									"GlossSeeAlso": ["GML", "XML"]
			                    },
								"GlossSee": "markup"
			                }
			            }
			        }
			    }
			};
			write("test1.json");
			print(self);
			export("test1.csv");
		''')
		Assertions.assertNotNull(result)
		val errors = result.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		val compiled = PythonCompiler.pythonCompile(result);
		val outputStream = new FileOutputStream("./test0.py")
		val strToBytes = compiled.getBytes();
		outputStream.write(strToBytes);
		outputStream.close();
		var s = ""
		val p = Runtime.getRuntime().exec("python3 test0.py");
		val br = new BufferedReader(
			new InputStreamReader(p.getInputStream()));
		while ((s = br.readLine()) !== null)
		System.out.println(s);
		p.waitFor();
		System.out.println ("exit: " + p.exitValue());
		p.destroy();
		}
		
		
	@Test
	def void Test1() {
		val result = parseHelper.parse('''			
			open "test1.json" in {
				print(self);
				print(
					select(self, "glossary"/"GlossDiv"/"title")
				);
				print(self);
				select(self, "glossary");
				print(self);
			};
		''')
		Assertions.assertNotNull(result)
		val errors = result.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		val compiled = PythonCompiler.pythonCompile(result);
		val outputStream = new FileOutputStream("./test1.py")
		val strToBytes = compiled.getBytes();
		outputStream.write(strToBytes);
		outputStream.close();
		var s = ""
		val p = Runtime.getRuntime().exec("python3 test1.py");
		val br = new BufferedReader(
			new InputStreamReader(p.getInputStream()));
		while ((s = br.readLine()) !== null)
		System.out.println(s);
		p.waitFor();
		p.destroy();		
		}
}